import {mapObjIndexed,forEachObjIndexed,map,clone,equals} from 'ramda'
import {adjaTables,partAlphabet} from './tables'
var testFunct = (obj)=> {
  partAlphabet.map(e => 
    e in obj ? obj[e] = obj[e] : obj[e] = {nextRoute: '-', dist:null}
  )
  return obj
}
function stuff(current,key,distance,neighbor){
  if(neighbor.dist !== null & (neighbor.dist + distance < current.dist || current.dist===null)){
    return {...current, dist:distance+neighbor.dist, nextRoute:key}
  }
  return current
} 
var iteration = (num,keyOuter,obj)=>{
  var result=num
  forEachObjIndexed((value,index) =>{
    result = mapObjIndexed((innerNum,keyInner,innerObj)=>{
      return stuff(innerNum,index,value.dist,obj[index][keyInner])
    },result)
  },originalAdjaTables[keyOuter])
  return result
}
const originalAdjaTables = clone(adjaTables)
var preparedTables = map(testFunct,adjaTables)
var count = 0
var keepGoing = true
var result=preparedTables
var previousIteration
while(keepGoing){
  count ++
  previousIteration = result
  result = mapObjIndexed(iteration,result)
  keepGoing = !equals(previousIteration, result)
}
console.log(result,"Stabil nach der ",count, ". runden")
